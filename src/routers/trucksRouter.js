const express = require('express');

const router = express.Router();
const {
  getDriverTrucks, addDriverTruck, getDriverTruck,
  updateDriverTruck, deleteDriverTruck, assignDriverTruck,
} = require('../controllers/trucksController');
const { authMiddleware } = require('../middleware/authMiddleware');
const { driverMiddleware } = require('../middleware/driverMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.get('/', authMiddleware, driverMiddleware, getDriverTrucks);
router.post('/', authMiddleware, driverMiddleware, asyncWrapper(addDriverTruck));
router.get('/:id', authMiddleware, driverMiddleware, asyncWrapper(getDriverTruck));
router.put('/:id', authMiddleware, driverMiddleware, asyncWrapper(updateDriverTruck));
router.delete('/:id', authMiddleware, driverMiddleware, asyncWrapper(deleteDriverTruck));
router.post('/:id/assign', authMiddleware, driverMiddleware, asyncWrapper(assignDriverTruck));

module.exports = {
  trucksRouter: router,
};
