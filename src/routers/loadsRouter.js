const express = require('express');

const router = express.Router();
const {
  getShipperLoads, addShipperLoad, getShipperLoad, updateShipperLoad,
  deleteShipperLoad, postShipperLoad, getDriverActiveLoads, iterateDriverLoadState, getShipperLoadShippingDetails,
} = require('../controllers/loadsController');
const { authMiddleware } = require('../middleware/authMiddleware');
const { shipperMiddleware } = require('../middleware/shipperMiddleware');
const { driverMiddleware } = require('../middleware/driverMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.get('/', authMiddleware, shipperMiddleware, asyncWrapper(getShipperLoads));
router.post('/', authMiddleware, shipperMiddleware, asyncWrapper(addShipperLoad));
router.get('/active', authMiddleware, driverMiddleware, asyncWrapper(getDriverActiveLoads));
router.patch('/active/state', authMiddleware, driverMiddleware, asyncWrapper(iterateDriverLoadState));
router.get('/:id', authMiddleware, shipperMiddleware, asyncWrapper(getShipperLoad));
router.put('/:id', authMiddleware, shipperMiddleware, asyncWrapper(updateShipperLoad));
router.delete('/:id', authMiddleware, shipperMiddleware, asyncWrapper(deleteShipperLoad));
router.post('/:id/post', authMiddleware, shipperMiddleware, asyncWrapper(postShipperLoad));
router.get('/:id/shipping_info', authMiddleware, shipperMiddleware, asyncWrapper(getShipperLoadShippingDetails));

module.exports = {
  loadsRouter: router,
};
