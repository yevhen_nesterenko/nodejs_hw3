const express = require('express');

const router = express.Router();
const { getUserInfo, deleteUserInfo, updateUserInfo } = require('../controllers/usersController');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.get('/', authMiddleware, getUserInfo);
router.delete('/', authMiddleware, deleteUserInfo);
router.patch('/password', authMiddleware, asyncWrapper(updateUserInfo));

module.exports = {
  usersRouter: router,
};
