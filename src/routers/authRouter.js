const express = require('express');

const router = express.Router();
const { registerUser, loginUser, forgotPasswordUser } = require('../controllers/authController');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/register', asyncWrapper(registerUser));

router.post('/login', loginUser);

router.post('/forgot_password', forgotPasswordUser);

module.exports = {
  authRouter: router,
};
