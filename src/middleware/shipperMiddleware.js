const shipperMiddleware = (req, res, next) => {
  const { role } = req.user;
  if (role !== 'SHIPPER') {
    return res.status(400).json({
      message: 'This request is only available for SHIPPER',
    });
  }
  return next();
};

module.exports = { shipperMiddleware };
