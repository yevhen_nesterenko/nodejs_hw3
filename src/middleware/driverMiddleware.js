const driverMiddleware = (req, res, next) => {
  const { role } = req.user;
  if (role !== 'DRIVER') {
    return res.status(400).json({
      message: 'This request is only available for DRIVER',
    });
  }
  return next();
};

module.exports = { driverMiddleware };
