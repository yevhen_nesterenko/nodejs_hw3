const {
  saveLoad, findLoads, findLoad, updateLoad, deleteLoad, findDriverForLoad, updateLogsInLoad,
} = require('../services/loadsService');
const {
  findTruck, updateTruck, findActiveLoad, iterateLoadState,
} = require('../services/trucksService');
const { loadJoiSchema } = require('../models/Loads');
const statesOfLoad = require('../dictionaries/statesOfLoad');

const getShipperLoads = async (req, res) => {
  const shipperLoads = await findLoads(req.user.userId);
  if (!shipperLoads) {
    return res.status(400).json({
      message: 'This shipper does not have loads',
    });
  }
  const { status, limit = 10, offset = 0 } = req.query;
  if (limit > 50 || limit < 0) {
    return res.status(400).json({
      message: 'Limit must be not more then 50 and not less then 0',
    });
  }
  if (!status) {
    return res.json({
      loads: shipperLoads.splice(offset, limit),
    });
  }
  const loadStatuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
  if (loadStatuses.indexOf(status) === -1) {
    return res.status(400).json({
      message: 'Wrong specified status',
    });
  }
  const filteredLoadsByStatus = shipperLoads.filter((load) => load.status === status);
  return res.json({
    loads: filteredLoadsByStatus.splice(offset, limit),
  });
};

const addShipperLoad = async (req, res) => {
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  await loadJoiSchema.validateAsync({
    name, payload, pickup_address, delivery_address, dimensions,
  });
  const savedLoad = await saveLoad(req.user.userId, name, payload, pickup_address, delivery_address, dimensions);
  if (!savedLoad) {
    throw new Error();
  }
  return res.json({ message: 'Load created successfully' });
};

const getShipperLoad = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const shipperLoad = await findLoad(id);
    return res.json({
      load: shipperLoad,
    });
  } catch (err) {
    return res.status(400).json({
      message: 'This shipper does not have this load',
    });
  }
};

const updateShipperLoad = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const {
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    if (!name && !payload && !pickup_address && !delivery_address && !dimensions) {
      return res.status(400).json({
        message: 'Please specify updated fields parameter',
      });
    }
    const shipperLoad = await findLoad(id);
    if (shipperLoad.status !== 'NEW') {
      return res.status(400).json({
        message: `This load can't update because status is '${shipperLoad.status}'`,
      });
    }
    const updatedTruck = await updateLoad(id, req.body);
    if (!updatedTruck) {
      throw new Error();
    }
    return res.json({ message: 'Load details changed successfully' });
  } catch (err) {
    return res.status(400).json({
      message: 'This shipper does not have this load',
    });
  }
};

const deleteShipperLoad = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const shipperLoad = await findLoad(id);
    if (shipperLoad.status !== 'NEW') {
      return res.status(400).json({
        message: `This load can't delete because status is '${shipperLoad.status}'`,
      });
    }
    const deletedLoad = await deleteLoad(id);
    if (!deletedLoad) {
      throw new Error();
    }
    return res.json({ message: 'Load deleted successfully' });
  } catch (err) {
    return res.status(400).json({
      message: 'This driver does not have this truck',
    });
  }
};

const postShipperLoad = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const load = await findLoad(id);
    if (load.status !== 'NEW') {
      return res.status(400).json({
        message: 'This load has already assigned by driver',
      });
    }
    const postedLoad = await updateLoad(id, { status: 'POSTED' });
    const appropriateTruck = await findDriverForLoad(postedLoad);
    if (!appropriateTruck) {
      const notPostedLoad = await updateLoad(id, { status: 'NEW' });
      const loadWithUpdatedLogs = await updateLogsInLoad(id, 'No driver found for the given parameters');
      if (!notPostedLoad || !loadWithUpdatedLogs) {
        throw new Error();
      }
      return res.json({
        message: 'Load posted successfully',
        driver_found: false,
      });
    }
    const assignedLoad = await updateLoad(id, { assigned_to: appropriateTruck._id, status: 'ASSIGNED', state: statesOfLoad[0] });
    const loadWithUpdatedLogs = await updateLogsInLoad(id, `Load assigned to driver with id ${appropriateTruck.assigned_to}`);
    const updatedTruckStatus = await updateTruck(appropriateTruck._id, { status: 'OL' });
    if (!assignedLoad || !loadWithUpdatedLogs || !updatedTruckStatus) {
      throw new Error();
    }
    return res.json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    return res.status(400).json({
      message: 'This shipper does not have this load',
    });
  }
};

const getDriverActiveLoads = async (req, res) => {
  const driverActiveLoad = await findActiveLoad(req.user.userId);
  if (!driverActiveLoad || driverActiveLoad.status === 'SHIPPED') {
    return res.status(400).json({
      message: 'This driver does not have active load',
    });
  }
  return res.json({
    load: driverActiveLoad,
  });
};

const iterateDriverLoadState = async (req, res) => {
  const driverActiveLoad = await findActiveLoad(req.user.userId);
  if (!driverActiveLoad) {
    return res.status(400).json({
      message: 'This driver does not have active load',
    });
  }
  if (driverActiveLoad.status === 'SHIPPED') {
    return res.status(400).json({
      message: 'This driver has already finished this load',
    });
  }
  const iteratedLoadState = await iterateLoadState(req.user.userId);
  if (!iteratedLoadState) {
    throw new Error();
  }
  const loadWithUpdatedStateLog = await updateLogsInLoad(iteratedLoadState._id, `Load state changed to '${iteratedLoadState.state}'`);
  if (loadWithUpdatedStateLog.status === 'SHIPPED') {
    const loadWithUpdatedStatusLog = await updateLogsInLoad(iteratedLoadState._id, 'Load status changed to SHIPPED');
    if (!loadWithUpdatedStatusLog) {
      throw new Error();
    }
  }
  return res.json({
    message: `Load state changed to '${iteratedLoadState.state}'`,
  });
};

const getShipperLoadShippingDetails = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const shipperLoad = await findLoad(id);
    if (!shipperLoad.assigned_to) {
      return res.status(400).json({
        message: 'This shipper does not have active load',
      });
    }
    const loadTruck = await findTruck(shipperLoad.assigned_to);
    if (!loadTruck) {
      throw new Error();
    }
    return res.json({
      load: shipperLoad,
      truck: loadTruck,
    });
  } catch (err) {
    return res.status(400).json({
      message: 'This shipper does not have this load',
    });
  }
};

module.exports = {
  addShipperLoad,
  getShipperLoads,
  getShipperLoad,
  updateShipperLoad,
  deleteShipperLoad,
  postShipperLoad,
  getDriverActiveLoads,
  iterateDriverLoadState,
  getShipperLoadShippingDetails,
};
