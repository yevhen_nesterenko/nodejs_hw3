const bcrypt = require('bcryptjs');
const { userJoiSchema } = require('../models/Users');
const {
  saveUser, findUser, signJWT, generateUserPassword, sendNewPasswordToEmail,
} = require('../services/authService');

const registerUser = async (req, res) => {
  const { email, password, role } = req.body;
  await userJoiSchema.validateAsync({
    email, password,
  });

  const roles = ['SHIPPER', 'DRIVER'];
  if (roles.indexOf(role) === -1) {
    return res.status(400).json({ message: `Please specify 'role' parameter. It can be '${roles.join(', ')}'` });
  }

  const savedUser = await saveUser(email, password, role);
  if (!savedUser) {
    throw new Error();
  }

  return res.json({ message: 'Profile created successfully' });
};

const loginUser = async (req, res) => {
  const user = await findUser(req.body.email);
  if (!user) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  if (await bcrypt.compare(String(req.body.password), String(user.password))) {
    const jwtToken = signJWT(user._id, user.email, user.role);
    return res.json({ jwt_token: jwtToken });
  }
  return res.status(403).json({ message: 'Wrong password' });
};

const forgotPasswordUser = async (req, res) => {
  const user = await findUser(req.body.email);
  if (!user) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  const newPassword = await generateUserPassword(user);
  sendNewPasswordToEmail(req.body.email, newPassword);
  return res.json({ message: 'New password sent to your email address' });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPasswordUser,
};
