const typesOfTrucks = require('../dictionaries/typesOfTrucks');
const {
  saveTruck, findTrucks, findTruck, updateTruck, deleteTruck,
} = require('../services/trucksService');

const getDriverTrucks = async (req, res) => {
  const driverTrucks = await findTrucks(req.user.userId);
  if (!driverTrucks) {
    return res.status(400).json({
      message: 'This driver does not have trucks',
    });
  }
  return res.json({
    trucks: driverTrucks,
  });
};

const addDriverTruck = async (req, res) => {
  const { type } = req.body;
  if (!type) {
    return res.status(400).json({
      message: "Please specify 'type' parameter",
    });
  }
  if (Object.keys(typesOfTrucks).indexOf(type.split(' ').join('_')) === -1) {
    return res.status(400).json({
      message: 'This type of track does not exist',
    });
  }
  const savedTruck = await saveTruck(req.user.userId, type);
  if (!savedTruck) {
    throw new Error();
  }
  return res.json({ message: 'Truck created successfully' });
};

const getDriverTruck = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const driverTruck = await findTruck(id);
    return res.json({
      truck: driverTruck,
    });
  } catch (err) {
    return res.status(400).json({
      message: 'This driver does not have this truck',
    });
  }
};

const updateDriverTruck = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const { type } = req.body;
    if (!type) {
      return res.status(400).json({
        message: "Please specify 'type' parameter",
      });
    }
    if (Object.keys(typesOfTrucks).indexOf(type.split(' ').join('_')) === -1) {
      return res.status(400).json({
        message: 'This type of track does not exist',
      });
    }
    const driverTruck = await findTruck(id);
    if (driverTruck.assigned_to !== null) {
      return res.status(400).json({
        message: "This truck can't update because it is assigned",
      });
    }
    const updatedTruck = await updateTruck(id, { type });
    if (!updatedTruck) {
      throw new Error();
    }
    return res.json({ message: 'Truck details changed successfully' });
  } catch (err) {
    return res.status(400).json({
      message: 'This driver does not have this truck',
    });
  }
};

const deleteDriverTruck = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const driverTruck = await findTruck(id);
    if (driverTruck.assigned_to !== null) {
      return res.status(400).json({
        message: "This truck can't delete because it is assigned",
      });
    }
    const deletedTruck = await deleteTruck(id);
    if (!deletedTruck) {
      throw new Error();
    }
    return res.json({ message: 'Truck deleted successfully' });
  } catch (err) {
    return res.status(400).json({
      message: 'This driver does not have this truck',
    });
  }
};

const assignDriverTruck = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  const { userId } = req.user;
  const driverTrucks = await findTrucks(userId);
  if (!driverTrucks.every((truck) => truck.assigned_to === null)) {
    const alreadyAssignedTruck = driverTrucks.find((truck) => truck.assigned_to !== null);
    if (String(alreadyAssignedTruck._id) === id) {
      return res.status(400).json({
        message: 'This car has already assigned',
      });
    }
    return res.status(400).json({
      message: 'You can assign only one truck',
    });
  }
  try {
    const assignedTruck = await updateTruck(id, { assigned_to: userId });
    if (!assignedTruck) {
      throw new Error();
    }
    return res.json({
      message: 'Truck assigned successfully',
    });
  } catch (err) {
    return res.status(400).json({
      message: 'This driver does not have this truck',
    });
  }
};

module.exports = {
  addDriverTruck,
  getDriverTrucks,
  getDriverTruck,
  updateDriverTruck,
  deleteDriverTruck,
  assignDriverTruck,
};
