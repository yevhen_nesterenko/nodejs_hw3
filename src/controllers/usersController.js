const bcrypt = require('bcryptjs');
const { findUserInfo, deleteUser, updateUserPassword } = require('../services/usersService');

const getUserInfo = async (req, res) => {
  const userInfo = await findUserInfo(req.user.userId);
  if (!userInfo) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  const {
    _id, role, email, created_date,
  } = userInfo;
  return res.json({
    user: {
      _id, role, email, created_date,
    },
  });
};

const deleteUserInfo = async (req, res) => {
  const deletedUser = await deleteUser(req.user.userId);
  if (!deletedUser) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  return res.json({ message: 'Profile deleted successfully' });
};

const updateUserInfo = async (req, res) => {
  const oldUserInfo = await findUserInfo(req.user.userId);
  if (!oldUserInfo) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  const { oldPassword, newPassword } = req.body;
  if (await bcrypt.compare(String(oldPassword), String(oldUserInfo.password))) {
    const newUserInfo = await updateUserPassword(newPassword, oldUserInfo);
    if (!newUserInfo) {
      throw new Error();
    }
    return res.json({ message: 'Password changed successfully' });
  }
  return res.status(400).json({ message: 'Wrong old password' });
};

module.exports = {
  getUserInfo,
  deleteUserInfo,
  updateUserInfo,
};
