const mongoose = require('mongoose');

const { Schema } = mongoose;

const truckSchema = new Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
  },
  created_date: {
    type: Date,
    required: true,
  },
});

const Truck = mongoose.model('Truck', truckSchema);
module.exports = {
  Truck,
};
