const mongoose = require('mongoose');
const Joi = require('joi');

const { Schema } = mongoose;

const userJoiSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),

  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required(),
});

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    required: true,
  },
});

const User = mongoose.model('Users', userSchema);
module.exports = {
  User,
  userJoiSchema,
};
