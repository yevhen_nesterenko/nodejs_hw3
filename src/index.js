const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect(process.env.DB_CONN);

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  try {
    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: err.message });
  next();
}

app.use(errorHandler);
