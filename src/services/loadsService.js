const { Load } = require('../models/Loads');
const { Truck } = require('../models/Trucks');
const typesOfTrucks = require('../dictionaries/typesOfTrucks');

const saveLoad = async (created_by, name, payload, pickup_address, delivery_address, dimensions) => {
  const load = new Load({
    created_by,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_date: new Date(),
  });

  const savedLoad = await load.save();

  return savedLoad;
};

const findLoads = async (id) => {
  const driverLoads = await Load.find({ created_by: id }, '-__v');
  return driverLoads;
};

const findLoad = async (id) => {
  const shipperLoad = await Load.findById(id, '-__v');
  return shipperLoad;
};

const updateLoad = async (id, updatedFields) => {
  const load = await Load.findById(id);
  Object.keys(updatedFields).forEach((field) => {
    let updatingValue = updatedFields[field];
    if (field === 'dimensions') {
      const sizes = { ...load[field] };
      Object.keys(updatedFields[field]).forEach((size) => {
        sizes[size] = updatedFields[field][size];
      });
      updatingValue = sizes;
    }
    load[field] = updatingValue;
  });
  const updatedLoad = await load.save();
  return updatedLoad;
};

const deleteLoad = async (id) => {
  const deletedLoad = await Load.findByIdAndDelete(id);
  return deletedLoad;
};

const findDriverForLoad = async (postedLoad) => {
  const isTrucks = await Truck.find({ status: 'IS' });
  const assignedTrucks = isTrucks.filter((truck) => truck.assigned_to);
  const { width: neededWidth, length: neededLength, height: neededHeight } = postedLoad.dimensions;
  const neededPayload = postedLoad.payload;
  function filterByPayloadAndDimensions(item) {
    const { payload, dimensions: { width, length, height } } = typesOfTrucks[item.type.split(' ').join('_')];
    return payload >= neededPayload && width >= neededWidth && length >= neededLength && height >= neededHeight;
  }
  const aproppriateTrucks = assignedTrucks.filter((truck) => filterByPayloadAndDimensions(truck));
  const aproppriateTruck = aproppriateTrucks[0];
  return aproppriateTruck;
};

const updateLogsInLoad = async (id, message) => {
  const load = await Load.findById(id);
  load.logs.push({ message, time: new Date() });
  const updatedLoad = await load.save();
  return updatedLoad;
};

module.exports = {
  saveLoad,
  findLoads,
  findLoad,
  updateLoad,
  deleteLoad,
  findDriverForLoad,
  updateLogsInLoad,
};
