const bcrypt = require('bcryptjs');
const { User, userJoiSchema } = require('../models/Users');

require('dotenv').config();

const findUserInfo = async (id) => {
  const userInfo = await User.findById(id, '-__v');
  return userInfo;
};

const deleteUser = async (id) => {
  const deletedUser = await User.findByIdAndDelete(id);
  return deletedUser;
};

const updateUserPassword = async (newPassword, oldUserInfo) => {
  await userJoiSchema.validateAsync({
    email: oldUserInfo.email, password: newPassword,
  });
  oldUserInfo.password = await bcrypt.hash(newPassword, 10);
  const newUserInfo = await oldUserInfo.save();
  return newUserInfo;
};

module.exports = {
  findUserInfo,
  deleteUser,
  updateUserPassword,
};
