const { Truck } = require('../models/Trucks');
const { Load } = require('../models/Loads');
const statesOfLoad = require('../dictionaries/statesOfLoad');

const saveTruck = async (created_by, type) => {
  const truck = new Truck({
    created_by,
    type,
    created_date: new Date(),
  });

  const savedTruck = await truck.save();

  return savedTruck;
};

const findTrucks = async (id) => {
  const driverTrucks = await Truck.find({ created_by: id }, '-__v');
  return driverTrucks;
};

const findTruck = async (id) => {
  const driverTruck = await Truck.findById(id, '-__v');
  return driverTruck;
};

const updateTruck = async (id, updatedFields) => {
  const truck = await Truck.findById(id, '-__v');
  Object.keys(updatedFields).forEach((field) => {
    truck[field] = updatedFields[field];
  });
  const updatedTruck = await truck.save();
  return updatedTruck;
};

const deleteTruck = async (id) => {
  const deletedTruck = await Truck.findByIdAndDelete(id);
  return deletedTruck;
};

const findActiveLoad = async (id) => {
  const driverAssignedTruck = await Truck.findOne({ assigned_to: id });
  const driverActiveLoad = await Load.findOne({ assigned_to: driverAssignedTruck._id }, '-__v');
  return driverActiveLoad;
};

const iterateLoadState = async (id) => {
  const driverAssignedTruck = await Truck.findOne({ assigned_to: id });
  const driverActiveLoad = await Load.findOne({ assigned_to: driverAssignedTruck._id });
  driverActiveLoad.state = statesOfLoad[statesOfLoad.indexOf(driverActiveLoad.state) + 1];
  if (driverActiveLoad.state === statesOfLoad[statesOfLoad.length - 1]) {
    driverActiveLoad.status = 'SHIPPED';
    const updatedTruck = await updateTruck(driverAssignedTruck._id, { status: 'IS' });
    if (!updatedTruck) {
      throw new Error();
    }
  }
  const iteratedLoadState = await driverActiveLoad.save();
  return iteratedLoadState;
};

module.exports = {
  saveTruck,
  findTrucks,
  findTruck,
  updateTruck,
  deleteTruck,
  findActiveLoad,
  iterateLoadState,
};
