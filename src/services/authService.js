const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const generator = require('generate-password');
const nodemailer = require('nodemailer');
const { User } = require('../models/Users');

require('dotenv').config();

const saveUser = async (email, password, role) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
    created_date: new Date(),
  });

  const savedUser = await user.save();

  return savedUser;
};

const findUser = async (email) => {
  const user = await User.findOne({ email });
  return user;
};

const signJWT = (id, email, role) => {
  const payload = { userId: id, email, role };
  const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
  return jwtToken;
};

const generatePassword = () => {
  const password = generator.generate({
    length: 10,
    numbers: true,
  });
  return password;
};

const generateUserPassword = async (user) => {
  const newPassword = generatePassword();
  user.password = await bcrypt.hash(newPassword, 10);
  const updatedUser = await user.save();
  if (!updatedUser) {
    throw new Error();
  }
  return newPassword;
};

const sendNewPasswordToEmail = (userEmail, password) => {
  const transporter = nodemailer.createTransport({
    service: 'hotmail',
    auth: {
      user: 'volnovakha-123-123@outlook.com',
      pass: '0987654321q',
    },
  });
  const options = {
    from: 'volnovakha-123-123@outlook.com',
    to: userEmail,
    subject: 'NEW PASSWORD',
    text: password,
  };
  const sendedMail = transporter.sendMail(options, (err) => {
    if (err) {
      throw new Error();
    }
  });
  return sendedMail;
};

module.exports = {
  saveUser,
  findUser,
  signJWT,
  generatePassword,
  generateUserPassword,
  sendNewPasswordToEmail,
};
