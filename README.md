# REST API Uber
This is UBER like service for freight trucks, in REST style, using MongoDB as database. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. Application contains 2 roles, driver and shipper.
The entire application is contained within the `src` directory.

`.env` use for configuration, anyone should be able to run this project with config defined in this file.

`.eslint.json` use for configuration coding format.

`package.json` use for records important metadata about a project and functional attributes of a project that npm uses to install dependencies.

`openapi.yaml` use for contain API documentation. Use [Swagger](https://swagger.io) to upload it.

## Install

    npm i

## Run the app

    npm run start

## Debug the app

    npm run debug

## Additional software

- Node.js
- MongoDB
- Postman (not necessary)